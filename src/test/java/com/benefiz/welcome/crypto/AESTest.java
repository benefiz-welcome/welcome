package com.benefiz.welcome.crypto;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class AESTest {

    @Test
    public void should_encrypt_my_message() {
        final String encryptedMessage = new AES().encrypt("should encrypt my message", "with my secret key");
        assertThat(encryptedMessage, equalTo("J/vaT5YRPZzFMhbZ910qBAhXTjlUDRF4OeseOt2veYs="));
    }

    @Test
    public void should_decrypt_my_message() throws BadSecretException {
        final String encryptedMessage = new AES().decrypt("Y1vxEWEebHdLaRHZk3rT1ZVo/kSNsH3MKLQrDGCoksk9cn9pHpq1qcSse67c1NDQGa9gJ2xKn1yAxVOy6otvSBJStgngWrPTzN+6tULNX88VDtEe2Bae4XZujfJJkYrRonIO0rlZl5nKCWXlNChw9g==", "with my secret key");
        assertThat(encryptedMessage, equalTo("junit"));
    }

}