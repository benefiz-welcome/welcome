package com.benefiz.welcome.crypto;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

/**
 * AES Utility class
 */
public class AES {

    private final String CIPHER_ALGO = "AES/ECB/PKCS5Padding";

    public String encrypt(final String strToEncrypt, final String secret) {
        try {
            Cipher cipher = Cipher.getInstance(CIPHER_ALGO);
            cipher.init(Cipher.ENCRYPT_MODE, getSecretKey(secret));
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes(StandardCharsets.UTF_8)));
        } catch (final Exception e) {
            System.out.println("Error while encrypting: " + e.toString());
            throw new CryptoTechnicalException(e);
        }
    }

    public String decrypt(final String strToDecrypt, final String secret) throws BadSecretException {
        try {
            Cipher cipher = Cipher.getInstance(CIPHER_ALGO);
            cipher.init(Cipher.DECRYPT_MODE, getSecretKey(secret));
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
        } catch (final BadPaddingException e) {
            throw new BadSecretException();
        } catch (final Exception e) {
            System.out.println("Error while decrypting: " + e.toString());
            throw new CryptoTechnicalException(e);
        }
    }

    private SecretKeySpec getSecretKey(final String myKey) throws NoSuchAlgorithmException {
        byte[] key = myKey.getBytes(StandardCharsets.UTF_8);
        MessageDigest sha = MessageDigest.getInstance("SHA-1");
        key = sha.digest(key);
        key = Arrays.copyOf(key, 16);
        return new SecretKeySpec(key, "AES");
    }
}