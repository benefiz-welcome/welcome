package com.benefiz.welcome.crypto;

public class CryptoTechnicalException extends RuntimeException {
    public CryptoTechnicalException(final Throwable cause) {
        super(cause);
    }
}
