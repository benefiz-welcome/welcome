package com.benefiz.welcome;

import com.benefiz.welcome.crypto.AES;
import com.benefiz.welcome.crypto.BadSecretException;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Scanner;

public class WelcomeApplication {

    public static void main(final String... args) throws IOException {
        final Scanner input = new Scanner(System.in);

        System.out.println("Pour découvrir ce qui se cache ici, donne nous le prénom et le nom de la personne que l'on cherche. Qui est-ce ? Pour le savoir, il va falloir trouver l'indice.");
        final String secretKey = input.nextLine();
        final String encryptedString = new String(WelcomeApplication.class.getResourceAsStream("/encrypted.txt").readAllBytes());

        try {
            final String decryptedString = new AES().decrypt(encryptedString, StringUtils.stripAccents(secretKey).toLowerCase().replaceAll(" ",""));
            System.out.println(decryptedString);
        } catch (BadSecretException e) {
            System.err.println("Ah non, ça n'était pas elle !");
        }
    }

}
